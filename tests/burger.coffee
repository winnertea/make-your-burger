# Copyright (C) 2017 A. Pielucha, M. Perin, A. Petitcol, M. Jolly
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
# OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

should = require 'should'

model = require('../app/db')('burger')

describe 'Burger', () ->
  random = Date.now()

  it 'Get burger when not created', (done) ->
    model.get random, (err, data) ->
      should.not.exist err

      should.not.exist data.name
      should.not.exist data.salade
      should.not.exist data.tomates
      should.not.exist data.steak
      should.not.exist data.oignons
      should.not.exist data.cornichons
      should.not.exist data.fromage

      done()

  it 'Save burger', (done) ->
    burger =
      name: random
      salade: 1
      tomates: 1
      steak: 1
      oignons: 0
      cornichons: 1
      fromage: 1

    model.save burger, (err) ->
      should.not.exist err
      done()

  it 'Get burger when created', (done) ->
    model.get random, (err, data) ->
      should.not.exist err
      data.should.have.property('salade', '1')
      data.should.have.property('tomates', '1')
      data.should.have.property('steak', '1')
      data.should.have.property('oignons', '0')
      data.should.have.property('cornichons', '1')
      data.should.have.property('fromage', '1')
      done()
