// Copyright (C) 2017 A. Pielucha, M. Perin, A. Petitcol, M. Jolly
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

(function() {
  // Define global variables
  var total_price = 0;

  // Get DOM elements
  var form = document.getElementById('create');
  var name = document.getElementById('name');
  var checkboxes = document.querySelectorAll('input[type=checkbox]');
  var button = document.getElementById('confirm');
  var price = document.getElementById('price');



  // Define event listeners

  for (var i = 0, l = checkboxes.length; i < l; i++) {
    checkboxes[i].addEventListener('change', function() {
      toggleIngredientDisplay(this);
      price.innerText = calculatePrice(this);
    });
  }

  button.addEventListener('click', function(e) {
    e.preventDefault();
    checkAndSubmitForm();
  });



  // Define functions

  function toggleIngredientDisplay(obj) {
    // Get ingredient name
    var name = obj.getAttribute('id');
    // If got checked, remove hidden class, else add
    obj.checked
      ? document.getElementById('img-' + name).classList.remove('hidden')
      : document.getElementById('img-' + name).classList.add('hidden');
  }

  function calculatePrice(obj) {
    // Get the toggled object price
    var price = parseFloat(obj.getAttribute('data-price'));
    // Increase total by price if it got checked, else decrease by price
    total_price = obj.checked ? total_price + price : total_price - price;

    // Return the total_price
    return total_price;
  }

  function checkAndSubmitForm() {
    if (name.value != '' && parseFloat(price.innerText) > 0) {
      form.submit();
    } else {
      alert('Vous devez entrer votre nom et choisir au moins un ingrédient.')
    }
  }
})();
