// Copyright (C) 2017 A. Pielucha, M. Perin, A. Petitcol, M. Jolly
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
// OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

(function() {
  // Get DOM elements
  var salade = document.getElementById('salade');
  var tomates = document.getElementById('tomates');
  var steak = document.getElementById('steak');
  var oignons = document.getElementById('oignons');
  var cornichons = document.getElementById('cornichons');
  var fromage = document.getElementById('fromage');
  var price = document.getElementById('price');



  // Define functions

  function isTrue(s) {
    return (s == 'true');
  }

  function calculateTotalPrice() {
    var total_price = 0;

    total_price += isTrue(salade.innerText) ? 0.5 : 0;
    total_price += isTrue(tomates.innerText) ? 0.5 : 0;
    total_price += isTrue(steak.innerText) ? 5 : 0;
    total_price += isTrue(oignons.innerText) ? 0.5 : 0;
    total_price += isTrue(cornichons.innerText) ? 0.5 : 0;
    total_price += isTrue(fromage.innerText) ? 2 : 0;

    return total_price;
  }



  // Execute anyway
  price.innerText = calculateTotalPrice();
})();
