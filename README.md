# Make your burger

*Software Quality 2 - ECE Paris (2017)*  
*Professor - Mr. HILLAH*  

Simple web application, on which you can design your own burger and order it.  

## Tools used

Our app is running on an **Express NodeJS** server, and coded in **CoffeeScript**.  
Front end part was made using **transpilers (Pug & Stylus)** and **Vanilla JS**.  
Versioning was setup using **Git**, and **Bitbucket**, where you can [find our repository](https://bitbucket.org/winnertea/make-your-burger).  
Coding was made exclusively on **Atom**, using the following packages :

* editorconfig
* atom-jade
* language-stylus
* linter-coffeelint
* teletype (peer-to-peer real-time collaboration)

We strongly advise you to use the same packages for a best experience (except teletype, which was just tested to work as a group in real-time).

## Installation

Ok, let's start now ! 😃  
To begin using our project, you have 2 options : install it **locally**, or use **Docker**.

### Local

To install it locally, you will need :

* a working computer
* your favorite terminal (bash, zsh, fish... heck it, even cmd if you want 😈)
* **Node** and **npm** installed and up-to-date (you can check with `node -v` and `npm -v`)

Now, open your terminal, `cd` to a directory where you want to clone our project, and do the following :
```shell
>   git clone git@bitbucket.org:winnertea/make-your-burger.git
>   npm install
>   npm start
```
And that's all ! Your app is now running, all you need to do is open a web browser, and go to http://127.0.0.1:8080 or https://127.0.0.1:8443.

### Docker

If, on the other hand, you prefer to use **Docker** and not install anything on your computer, then just open your terminal, and run these two simple commands (assuming you have installed Docker of course, you can check with `docker -v`) :
```shell
>   docker pull arno0/make-your-burger
>   docker run -it -p 8080:8080 -p 8443:8443 arno0/make-your-burger
```
The app is now up and running, inside a docker container. Now, just open a web browser, and go to http://127.0.0.1:8080 or https://127.0.0.1:8443.

By default, you just pulled the release of our app corresponding to the :latest tag, which is linked to our Git master branch.  
If you want to follow our newest features (linked to our Git develop branch, not guaranteed to be a stable version) you can do instead :
```shell
>   docker pull arno0/make-your-burger:develop
>   docker run -it -p 8080:8080 -p 8443:8443 arno0/make-your-burger:develop
```

## Lint & Tests

For our project, we used the npm package **coffeelint** to lint all our CoffeeScript code. We used custom rules, that you can see in the `coffeelint.json` file, trying to be as strict as possible to have a uniform code over our app. To verify that our code is well linted, you can type the following command at the root of the project (only if you installed the project locally) :
```
>   npm run lint
```

For testing, we used two node modules : **mocha** and **should**.  
We only tested our models (actually we only have one model) methods, testing the rest was not really useful.  
Tests can be found under the `tests` folder. To run the tests locally, you can run the following command :
```
>   npm test
```
If you're on docker, or you just don't want to run this command for a reason, we implemented Continuous Integration, with the Bitbucket CI/CD tool.  
Each time we commit & push, our code goes through a pipeline that runs all our tests and shows the output.  
You can see these pipelines by [clicking here](https://bitbucket.org/winnertea/make-your-burger/addon/pipelines/home#!/). Just click on one, and deploy the section under `npm test` to see the output.

## Design patterns

Last but not least, let's talk about the design patterns we used in our application.  
NodeJS and JavaScript in general allows to easily implement a bunch of design patterns.  
We used the following ones :

* **MVC** => Since we made a web app, the Model - View - Controller design pattern choice seemed obvious. Our server is launched in the `app/main.coffee` file, and setup in `app/server.coffee`. The routing is handled in `app/http/routes.coffee`, which links URIs to controllers, defined in the `app/http/controllers/` folder. They use models defined in the `app/models/` folder, and render views (written in pug) defined in the `resources/views/` folder.
* **Dependency Injection** => All our models are called through a single file `app/db.coffee`. This allows for only one database instance to be declared, in a centralized environment, and then passed on to the corresponding model. In the controller, you require the `db.coffee` file with a parameter being the name of the model you wish to retrieve. Then, in the `db.coffee` file, the database instance is created, and passed on to the corresponding model. There you go : a double dependency injection.
* **Factory** => With this centralized model management, the use for a factory was really obvious. When the controller needs a model, it calls the `app/db.coffee` file, with the name of the required model as a parameter. In this file, you can find a `switch` block that takes the model name as a parameter, and returns the corresponding model to the controller. And that makes the Factory design pattern.

We also thought about implementing a Builder design pattern for building our burgers, using JS promises, but didn't have enough time to do it unfortunately.

## More

We didn't use a micro-services architecture for this project, by lack of time. We thought of doing so, by implementing the back end part on a NodeJS server, and a front end part using React. This would be nice for a possible update of the project.  

In terms of deployment, our whole app is dockerized (refer to the Installation on Docker part of this document), which makes it really easy to deploy on any kind of server (AWS, Azure, ...). We deployed it on the personal server of a person of our group, all that was needed was to run the specified commands, and the app was up and running under minutes.  

## Contributors

Marion Jolly, Marie Perin, Arnaud Petitcol, Alexandre Pielucha

## License (ISC)

Copyright (C) 2017 A. Pielucha, M. Perin, A. Petitcol, M. Jolly

Permission to use, copy, modify, and/or distribute this software for any  
purpose with or without fee is hereby granted, provided that the above  
copyright notice and this permission notice appear in all copies.  

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF  
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR  
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION  
OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN  
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
