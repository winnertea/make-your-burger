# Copyright (C) 2017 A. Pielucha, M. Perin, A. Petitcol, M. Jolly
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
# OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

model = require('../../db')('burger')

module.exports =
  index: (req, res) ->
    res.render 'index'

  create: (req, res) ->
    res.render 'create'

  store: (req, res) ->
    burger =
      name: req.body.name
      salade: req.body.salade?
      tomates: req.body.tomates?
      steak: req.body.steak?
      oignons: req.body.oignons?
      cornichons: req.body.cornichons?
      fromage: req.body.fromage?

    model.save burger, () ->
      res.redirect "/confirmation?name=#{req.body.name}"

  confirmation: (req, res) ->
    if not req.query.name?
      res.redirect '/'
    else
      model.get req.query.name, (err, data) ->
        throw err if err

        res.render 'confirmation',
          burger: data
