# Copyright (C) 2017 A. Pielucha, M. Perin, A. Petitcol, M. Jolly
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
# OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

module.exports = (db) ->
  all: (callback) ->
    burgers = []

    # Connect to db and open read stream
    rs = db.createReadStream
      gte: 'burgers:!'
      lte: 'burgers:~'
    # Handle events
    rs.on 'data', (data) ->
      [ _, name ] = data.key.split ':'
      [ salade,
        tomates,
        steak,
        oignons,
        cornichons,
        fromage ] = data.value.split ':'

      burgers.push
        name: name
        salade: salade
        tomates: tomates
        steak: steak
        oignons: oignons
        cornichons: cornichons
        fromage: fromage

    rs.on 'error', callback
    rs.on 'close', () ->
      callback null, burgers

  save: (burger, callback) ->
    # Connect to db and open write stream
    ws = db.createWriteStream()

    # Get params
    { name, salade, tomates, steak, oignons, cornichons, fromage } = burger

    # Save user in db
    data =
      key: "burgers:#{name}"
      value: "#{salade}:#{tomates}:#{steak}:#{oignons}:#{cornichons}:#{fromage}"
    ws.write data

    # Handle errors & close
    ws.on 'error', callback
    ws.on 'close', callback

    # Close stream
    ws.end()

  get: (name, callback) ->
    burger = {}

    # Connect to db and open read stream
    rs = db.createReadStream
      gte: "burgers:#{name}"
      lte: "burgers:#{name}"
    # Handle events
    rs.on 'data', (data) ->
      [ salade,
        tomates,
        steak,
        oignons,
        cornichons,
        fromage ] = data.value.split ':'

      burger =
        name: name
        salade: salade
        tomates: tomates
        steak: steak
        oignons: oignons
        cornichons: cornichons
        fromage: fromage

    rs.on 'error', callback
    rs.on 'close', () ->
      callback null, burger
